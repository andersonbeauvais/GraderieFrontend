import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import router from './router/index.js'
import store from "./store/index"
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faEnvelope, faCheck, faLock, faMapMarkerAlt, faPhone, faUser, faThumbsUp
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(faEnvelope, faCheck, faLock, faMapMarkerAlt, faPhone, faUser, faThumbsUp)
Vue.component('font-awesome-icon', FontAwesomeIcon);

require('@/assets/main.scss');

Vue.config.productionTip = false
Vue.use(VueRouter)



new Vue({
  render: h => h(App),
  router,
  store,
}).$mount('#app')
