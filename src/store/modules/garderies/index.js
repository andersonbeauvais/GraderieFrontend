import axios from 'axios'
const apiUrl = "http://127.0.0.1:8000/api/";

export default {
    namespaced: true,
  
    state() {
      return {
        toutesLesGarderies: [],
      };
    },
    actions: {
      getAllGarderies({commit}){
        axios
        .get(`${apiUrl}garderie/list`)
        .then((response) => {
          const garderies = response.data;
          commit("UPDATE_GARDERIES", garderies);
        })
        .catch((error) => console.log(error));
      },
    },
    mutations: {
      UPDATE_GARDERIES(state, garderies) {
        this.state.garderies.toutesLesGarderies = garderies;
      },
    },
  };