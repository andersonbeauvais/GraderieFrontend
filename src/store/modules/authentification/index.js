import axios from "axios";
const apiUrl = "http://127.0.0.1:8000/api/";
export default {
  namespaced: true,

  state() {
    return {
      currentUser: null,
      currentUserAccessToken: null,
    };
  },
  actions: {
    changeCurrentUser({ commit }, { newUser, token }) {
      commit("UPDATE_USER", newUser);
      commit("UPDATE_TOKEN", token);
    },
    logoutCurrentUser({ commit }, token) {
      axios({
        method: "get",
        url: `${apiUrl}auth/logout`,
        headers: {
          Authorization: `Bearer ${token}`,
        },
        data: {},
      })
        .then(() => {
          commit("UPDATE_USER", null);
          commit("UPDATE_TOKEN", null)
        })
        .catch((error) => console.log(error));
    },
  },

  mutations: {
    UPDATE_USER(state, newUser) {
      this.state.authentification.currentUser = newUser;
    },
    UPDATE_TOKEN(state, token) {
      this.state.authentification.currentUserAccessToken = token;
    }
  },
};
