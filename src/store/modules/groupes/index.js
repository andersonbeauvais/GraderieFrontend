import axios from "axios";
const apiUrl = "http://127.0.0.1:8000/api/";

export default {
  namespaced: true,

  state() {
    return {
      groupesDeLaGarderie: [],
      enfantsDuGroupe: [],
    };
  },
  actions: {
    getGroupesParIdGarderie({ commit }, IdGarderie) {
      axios
        .get(`${apiUrl}garderie/GroupeParGarderie?id=${IdGarderie}`)
        .then((response) => {
          const groupes = response.data;
          commit("UPDATE_GARDERIES", groupes);
        })
        .catch((error) => console.log(error));
    },
    getEnfantsParIdGroupe({ commit }, IdGroupe) {
      axios.get(`${apiUrl}enfantsByGroupe/${IdGroupe}`).then((response) => {
        const enfants = response.data;
        commit("UPDATE_ENFANTSDUGROUPE", enfants);
      });
    },
  },
  mutations: {
    UPDATE_GARDERIES(state, groupes) {
      this.state.groupes.groupesDeLaGarderie = groupes;
    },
    UPDATE_ENFANTSDUGROUPE(state, enfants) {
      this.state.groupes.enfantsDuGroupe = enfants;
    },
  },
};
