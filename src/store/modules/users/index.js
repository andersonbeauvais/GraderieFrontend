import axios from "axios";
const apiUrl = "http://127.0.0.1:8000/api/";

export default {
    namespaced: true,

    state() {
        return {
        };
    },
    actions: {
        editUserRole({ idUser, idRole }) {
            axios.put(`${apiUrl}updateIdRole/${idUser}?id_role=${idRole}`).catch((error) => console.log(error));
        },
    },
    mutations: {}
};