import axios from "axios";
const apiUrl = "http://127.0.0.1:8000/api/";
export default {
  namespaced: true,
  state() {
    return {
      currentEducatrice: null,
    };
  },
  actions: {
    getEducatriceParIdUser({ commit }, user_id) {
      axios.get(`${apiUrl}currentEducatrice/${user_id}`).then((response) => {
        commit("UPDATE_CURRENTEDUCATRICE", response.data);
      });
    },
  },
  mutations: {
    UPDATE_CURRENTEDUCATRICE(state, educatrice) {
      this.state.educatrices.currentEducatrice = educatrice;
    },
  },
};
