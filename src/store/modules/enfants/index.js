import axios from "axios";
const apiUrl = "http://127.0.0.1:8000/api/";
export default {
  namespaced: true,
  state() {
    return {};
  },
  actions: {
    updatePresenceEnfant({idEnfant, presence}) {
      axios
        .put(`${apiUrl}updatePresence/${idEnfant}?presence=${presence}`)
        .catch();
    },
  },
  mutations: {},
};
