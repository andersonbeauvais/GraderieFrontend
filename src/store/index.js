import Vue from "vue";
import Vuex from "vuex";
import authentification from "./modules/authentification";
import garderies from "./modules/garderies";
import groupes from "./modules/groupes";
import enfants from "./modules/enfants";
import parents from "./modules/parents";
import users from "./modules/users";
import createPersistedState from "vuex-persistedstate";
import educatrices from "./modules/educatrices";

Vue.use(Vuex);

export default new Vuex.Store({
  state() {
    return {};
  },
  actions: {},
  mutations: {},
  getters: {},
  strict: true,
  modules: {
    authentification,
    garderies,
    groupes,
    enfants,
    parents,
    users,
    educatrices,
  },
  plugins: [createPersistedState()],
});
