import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../components/Login.vue";
import SignUp from "../components/SignUp.vue";
import ChoixTypeUtilisateur from "../components/ChoixTypeUtilisateur.vue";
import InscriptionParent from "../components/InscriptionParent.vue";
import InscriptionEducatrice from "../components/InscriptionEducatrice.vue";
import LoginParent from "../components/LoginParent.vue";
import LoginEducatrice from "../components/LoginEducatrice.vue";
import LandingPage from "../components/LandingPage.vue";
import DossierMedicaleParent from "../components/DossierMedicaleParent.vue";
import DossierMedicaleEducatrice from "../components/DossierMedicaleEducatrice.vue";
import CreationRapport from "../components/CreationRapport.vue";
import ListeEnfants from "../components/ListeEnfants.vue";
import SuccessfullyLoggedIn from "../components/SuccessfullyLoggedIn";
import MonGroupe from "../components/MonGroupe.vue";

Vue.use(VueRouter);
const routes = [
  {
    path: "/",
    name: "Login",
    component: Login,
  },
  {
    path: "/SuccessfullyLoggedIn",
    name: "SuccessfullyLoggedIn",
    component: SuccessfullyLoggedIn,
  },

  {
    path: "/SignUp",
    name: "SignUp",
    component: SignUp,
  },
  {
    path: "/landing",
    name: "landing",
    component: LandingPage,
  },
  {
    path: "/MonGroupe",
    name: "MonGroupe",
    component: MonGroupe,
  },

  {
    path: "/ChoixTypeUtilisateur",
    name: "ChoixTypeUtilisateur",
    component: ChoixTypeUtilisateur,
  },

  {
    path: "/InscriptionParent",
    name: "InscriptionParent",
    component: InscriptionParent,
  },

  {
    path: "/InscriptionEducatrice",
    name: "InscriptionEducatrice",
    component: InscriptionEducatrice,
  },

  {
    path: "/LoginParent",
    name: "LoginParent",
    component: LoginParent,
  },

  {
    path: "/LoginEducatrice",
    name: "LoginEducatrice",
    component: LoginEducatrice,
  },

  {
    path: "/DossierMedicaleParent",
    name: "DossierMedicaleParent",
    component: DossierMedicaleParent,
  },

  {
    path: "/DossierMedicaleEducatrice",
    name: "DossierMedicaleEducatrice",
    component: DossierMedicaleEducatrice,
  },

  {
    path: "/CreationRapport",
    name: "CreationRapport",
    component: CreationRapport,
  },

  {
    path: "/ListeEnfants",
    name: "ListeEnfants",
    component: ListeEnfants,
  },

  {
    path: "/CreationDossierMedicale/:idEnfant",
    name: "CreationDossierMedicale",
    props: (route) => ({ idEnfant: Number(route.params.idEnfant) }),
    component: () => import('../components/CreationDossierMedicale.vue'),
  },


  {
    path: "/DossierMedicale/:idBilanSante",
    name: "DossierMedicale",
    props: (route) => ({ idBilanSante: String(route.params.idBilanSante) }),
    component: () => import('../components/DossierMedicale.vue'),
  },

];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
